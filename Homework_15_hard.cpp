﻿

#include <iostream>
#include <cmath>
#include <string>


template <typename T>
class Stack
{
private:
    int size = 2;
    int stack = 0;
    int currentSize = 0;
    T* arr = nullptr;
    T* newArrey = nullptr;

public:
    Stack()
    {
        arr = new T[size];
        currentSize = size;
    }

    Stack(T firstSteck)
    {
        currentSize = size;
        arr = new T[currentSize];
        arr[stack] = firstSteck;
        stack++;

    }

    T push(T newSteck)
    {
        if (stack >= 0 && stack < currentSize)
        {
            arr[stack] = newSteck;
            stack++;
        }

        else if (stack >= currentSize)
        {
            currentSize = currentSize + size;
            newArrey = new T[currentSize];
            for (int i = 0; i < stack; i++)
            {
                newArrey[i] = arr[i];
            }
            delete[] arr;
            arr = nullptr;
            arr = new T[currentSize];
            for (int i = 0; i < stack; i++)
            {
                arr[i] = newArrey[i];
            }
            delete[] newArrey;
            newArrey = nullptr;

            arr[stack] = newSteck;
            stack++;
        }
        return arr[stack - 1];
    }

    T pop()
    {

        if (stack <= currentSize - size)
        {
            currentSize = currentSize - size;
            newArrey = new T[currentSize];
            for (int i = 0; i < stack; i++)
            {
                newArrey[i] = arr[i];
            }
            delete[] arr;
            arr = nullptr;
            arr = new T[currentSize];
            for (int i = 0; i < stack; i++)
            {
                arr[i] = newArrey[i];
            }
            delete[] newArrey;
            newArrey = nullptr;
        }
        stack--;
        return arr[stack];
    }

    void GetSteck()
    {
        std::cout << "last stack number: " << stack << " ; " << "steck filling: " << arr[stack - 1] << "\n";
    }

    void GetAllSteck()
    {
        for (int i = 0, n = 1; i < stack; i++, n++)
        {
            std::cout << arr[i] << "\n";
        }
    }

    void GetCurrentSize()
    {
        std::cout << "current size: " << currentSize << "\n";
    }

    int GetLastStackNumber()
    {
        return stack;
    }

};




int main()
{
    setlocale(LC_ALL, "Russian");
    Stack<int> s;
    s.GetCurrentSize(); //проверка размера начального массива
    std::cout << "add stack number: " << s.GetLastStackNumber() << " ; " << "add stack filling: " << s.push(12) << "\n"; //проверка данных первого push
    s.push(52);
    s.push(61);
    s.push(33);
    s.push(94);
    s.GetSteck(); //проверка сохраниения данных о последнем стеке
    s.GetCurrentSize(); //проверка увеличения размера массива
    std::cout << "extractable stack number: " << s.GetLastStackNumber() << " ; " << "extractable stack filling: " << s.pop() << "\n"; //проверка данных первого pop
    s.GetSteck(); //проверка изменения информации о последнем стеке
    s.pop();
    s.pop();
    s.GetCurrentSize(); //проверка уменьшения размера массива
    s.GetSteck(); //проверка информации о последнего стеке
    s.GetAllSteck(); //проверка заполнения стека после всех операций

    Stack<std::string> str;
    str.GetCurrentSize(); //проверка размера начального массива
    std::cout << "add stack number: " << str.GetLastStackNumber() << " ; " << "add stack filling: " << str.push("один") << "\n"; //проверка данных первого push
    str.push("два");
    str.GetSteck();
    str.push("три");
    str.GetSteck();
    str.push("четыре");
    str.GetSteck();
    str.push("пять");
    str.GetSteck(); //проверка сохраниения данных о последнем стеке
    str.GetCurrentSize(); //проверка увеличения размера массива
    std::cout << "extractable stack number: " << str.GetLastStackNumber() << " ; " << "extractable stack filling: " << str.pop() << "\n"; //проверка данных первого pop
    str.GetSteck(); //проверка изменения информации о последнем стеке
    str.pop();
    str.pop();
    str.GetCurrentSize(); //проверка уменьшения размера массива
    str.GetSteck(); //проверка информации о последнего стеке
    str.GetAllSteck(); //проверка заполнения стека после всех операций

}